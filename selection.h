#ifndef SELECTION_H
#define SELECTION_H
#include <iostream>
using namespace std;


class Selection {
    private:
     
    public:
    
        Selection();

        void imprimir_vector(int a[], int n);
        void ordena_seleccion(int a[], int n);
        void ejecutar(int n, int a[],char *opc);
  
        
};
#endif