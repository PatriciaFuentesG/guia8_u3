
#include <iostream>
#include "selection.h"
#include "quicksort.h"
using namespace std;


void imprimir_vector(int a[], int n) {
  int i;

  for (i=0; i<n; i++) {
    cout<<"A["<<i<<"]="<<a[i]<<" ";
  }
  cout<<"\n";
}

int main(int argc, char **argv) {
// esto es para que el usuario escriba numeros >= a 2
  if (atoi(argv[1])<=1){
        cout << "Uso: \n./matriz n" << endl;
        return -1;
  }else{
   int n = atoi(argv[1]);
   int a[n],elemento;
   for (int i=0; i<n; i++) {//se genera una matriz con numeros aletorios
    elemento = (rand() % 5000) + 0;
    a[i] = elemento;
  } 
   cout<<"\n-----------------------------------------------"<<endl; 
   imprimir_vector(a,n);
   cout<<"-----------------------------------------------"<<endl;  
   Quicksort *quicksort = new Quicksort;
   Selection *selection = new Selection;

   quicksort->ejecutar(n,a,argv[2]);
 
   cout<<"\n-----------------------------------------------"<<endl; 
   selection->ejecutar(n,a,argv[2]);
   delete quicksort,selection;

 }


  return 0;
}
