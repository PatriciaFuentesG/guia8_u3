# guia8_u3

Para poder usar esta aplicación usted debe tener IDE con leguaje c++ como Geany o Visual Studio Code, instalado en su computador.
Primero debe descargar todos los elementos . Abra la IDE y y abra la carpeta recien descargada
en la consola puede ejecutar el programa al escribir "make".
En este programa usted puede comparar dos metodos de ordenamiento los cuales son Quicksort y Selection para crear vectores ordenados, para ejecutar este programa usted debe escribir :

        ./Comparacion N s   N-> representa un número mayor o igual a 2 que usted debe escribir.
                         s/n-> si usted escribe s significa que quiere ver los vertores ordenados , en caso de que no quiera verlos ponga n

Despues en pantalla usted vera el vertores original, con sus tiempos de cada metodo y en caso de haber escrito s tambien se mostraran los vertores ordenados en pantalla por ejemplo:

        ./Comparacion 3  s
        -----------------------------------------------
        A[0]=4383 A[1]=886 A[2]=2777 
        -----------------------------------------------
        Quicksort: 0 milisegundos
        A[0]=886 A[1]=2777 A[2]=4383 

        -----------------------------------------------
        Selection: 0 milisegundos
        A[0]=886 A[1]=2777 A[2]=4383


        ./Comparacion 2 n

        -----------------------------------------------
        A[0]=4383 A[1]=886 
        -----------------------------------------------
        Quicksort: 0 milisegundos

        -----------------------------------------------
        Selection: 0 milisegundos
 
 
 Eso es todo gracias por usar este programa
