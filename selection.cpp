#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include "selection.h"
using namespace std;

Selection::Selection(){}

void Selection::imprimir_vector(int a[], int n) {
  int i;

  for (i=0; i<n; i++) {
    cout<<"A["<<i<<"]="<<a[i]<<" ";
  }
  cout<<"\n";
}
//Aqui se ordenan los numeros con el metodo selection
void Selection::ordena_seleccion(int a[], int n) {
  int i, menor, k, j;
  
  for (i=0; i<=n-2; i++) {
    menor = a[i];
    k = i;
    
    for (j=i+1; j<=n-1; j++) {
      if (a[j] < menor) {
        menor = a[j];
        k = j;
      }
    }
    
    a[k] = a[i];
    a[i] = menor;
  }
}
//Aqui se ejecuta el metodo de ordenamiento
void Selection::ejecutar(int n,int a[],char *opc){
  int i;
  int elemento;

  clock_t t1, t2;
  double secs;
      
  t1 = clock();
  ordena_seleccion(a, n);
  t2 = clock();


  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
  //Se imprime el tiempo que tomo en para ordenarse
  printf("Selection: %.0f milisegundos\n",secs * 1000.0);
  string s = "s";
  if(0 == strcmp(s.c_str(), opc)){
  imprimir_vector(a, n); 
  }
}