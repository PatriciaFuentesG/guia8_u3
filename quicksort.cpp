#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include "quicksort.h"
using namespace std;

Quicksort::Quicksort(){}

void Quicksort::imprimir_vector(int a[], int n) {
  int i;

  for (i=0; i<n; i++) {
    cout<<"A["<<i<<"]="<<a[i]<<" ";
  }
  cout<<"\n";
}
//Aqui se ordenan los numeros con el metodo quicksort
void Quicksort::ordena_quicksort(int a[], int n) {
  int tope, ini, fin, pos;
  int pilamenor[100];
  int pilamayor[100];
  int izq, der, aux, band;
  
  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = n-1;
  
  while (tope >= 0) {
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    tope = tope - 1;
    
    // reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = true;
    
    while (band == true) {
      while ((a[pos] <= a[der]) && (pos != der))
        der = der - 1;
        
      if (pos == der) {
        band = false;
      } else {
        aux = a[pos];
        a[pos] = a[der];
        a[der] = aux;
        pos = der;
        
        while ((a[pos] >= a[izq]) && (pos != izq))
          izq = izq + 1;
          
        if (pos == izq) {
          band = false;
        } else {
          aux = a[pos];
          a[pos] = a[izq];
          a[izq] = aux;
          pos = izq;
        }
      }
    }
    
    if (ini < (pos - 1)) {
      tope = tope + 1;
      pilamenor[tope] = ini;
      pilamayor[tope] = pos - 1;
    }
    
    if (fin > (pos + 1)) {
      tope = tope + 1;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }
}
//Aqui se ejecuta el metodo de ordenamiento
void Quicksort::ejecutar(int n, int a[],char *opc){
  int i;
  int elemento;

  clock_t t1, t2;
  double secs;
    
  t1 = clock();
  ordena_quicksort(a, n);
  t2 = clock();

  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
  //Se imprime el tiempo que tomo en para ordenarse
  printf("Quicksort: %.0f milisegundos\n",secs * 1000.0);

  string s = "s";
  if(0 == strcmp(s.c_str(), opc)){
  imprimir_vector(a, n); 
  }

  
}