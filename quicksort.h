#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <iostream>
using namespace std;


class Quicksort {
    private:
     
    public:
        Quicksort();

        void imprimir_vector(int a[], int n);
        void ordena_quicksort(int a[], int n);
        void ejecutar(int n,int a[],char *opc);

        
};
#endif